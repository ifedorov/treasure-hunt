def load_table(filename):
    with open(filename) as fhandler:
        return [line.split() for line in fhandler.readlines()]


def find_treasure(array, position='11'):
    row, col = map(lambda x: int(x) - 1, position)
    if row < 0 or col < 0:
        raise IndexError
    next_position = array[row][col]
    if next_position != position:
        return [position] + find_treasure(array, next_position)
    return [position]
