class Treasure(object):
    def __init__(self, filepath=None):
        self.table = []
        if filepath:
            self.load_data(filepath=filepath)

    def load_data(self, filepath):
        self.table = []
        with open(filepath) as fhandler:
            for line_n, line in enumerate(fhandler.readlines(), 1):
                row = []
                for col_n, item in enumerate(line.split(), 1):
                    row.append(
                        Clue(row=line_n, col=col_n, value=item)
                    )
                self.table.append(row)

        if len(self.table) != 5 or self.get_length() != 25:
            raise WrongInitialData

    def get_length(self):
        count = 0
        for line in self.table:
            count = count + len(line)
        return count

    def get_next_clue(self, item=None):
        if item is None:
            return self.table[0][0]
        row, col = item.next_coordinates
        return self.table[row][col]

    def follow_by_clue(self):
        current = None
        length = self.get_length()
        counter = 0
        while True:
            next_clue = self.get_next_clue(item=current)
            if current == next_clue:
                break
            if counter == length:
                raise InfiniteLoop
            yield next_clue

            current = next_clue
            counter += 1

    def get_result(self):
        return [str(i) for i in self.follow_by_clue()]


class Clue(object):
    min_range = 11
    max_range = 55

    def __init__(self, row, col, value):
        self.row = row
        self.col = col
        self.value = value
        self.digit = int(value)
        if not self.min_range <= self.digit <= self.max_range:
            raise ClueOutOfRange

    def __eq__(self, other):
        if isinstance(other, Clue):
            return self.get_position == other.get_position
        return self.get_position == other

    @property
    def get_position(self):
        return "{}{}".format(self.row, self.col)

    @property
    def next_coordinates(self):
        row, col = self.value
        return int(row) - 1, int(col) - 1

    def __str__(self):
        return self.get_position


class WrongInitialData(Exception):
    pass


class ClueOutOfRange(Exception):
    pass


class InfiniteLoop(Exception):
    pass
