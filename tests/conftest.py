import pytest

FILEDATA = {
    'successful_first': [
        '11 11 11 11 11\n',
        '11 11 11 11 11\n',
        '11 11 11 11 11\n',
        '11 11 11 11 11\n',
        '11 11 11 11 11'
    ],
    'successful': [
        '12 13 14 15 21\n',
        '22 23 24 25 31\n',
        '32 33 34 35 41\n',
        '42 43 44 45 51\n',
        '52 53 54 55 55'
    ],
    'infinite': [
        '12 11 11 11 11\n',
        '11 11 11 11 11\n',
        '11 11 11 11 11\n',
        '11 11 11 11 11\n',
        '11 11 11 11 11'
    ],
    'clue_text': [
        'hello 11 11 11 11\n',
        '11 11 11 11 11\n',
        '11 11 11 11 11\n',
        '11 11 11 11 11\n',
        '11 11 11 11 11'
    ],
    'less11': ['10'],
    'bigger55': ['56'],
    'wrong_num_rows': ['12 13 14 15 12\n'],
    'wrong_num_cols': ['55\n', '55\n', '55\n', '55\n', '55\n'],
}


def generate_fixture(tmpdir, param):
    tmpfile = tmpdir.mktemp('data').join('{}.txt'.format(param))
    with tmpfile.open('w') as f:
        f.writelines(FILEDATA.get(param, []))
    return str(tmpfile)


@pytest.fixture(scope='module')
def fixture_file_successful(tmpdir_factory):
    return generate_fixture(tmpdir_factory, 'successful')


@pytest.fixture(scope='module')
def fixture_file_successful_first(tmpdir_factory):
    return generate_fixture(tmpdir_factory, 'successful_first')


@pytest.fixture(scope='module')
def fixture_file_infinite_loop(tmpdir_factory):
    return generate_fixture(tmpdir_factory, 'infinite')


@pytest.fixture(scope='module')
def fixture_file_clue_text(tmpdir_factory):
    return generate_fixture(tmpdir_factory, 'clue_text')


@pytest.fixture(scope='module')
def fixture_file_less11(tmpdir_factory):
    return generate_fixture(tmpdir_factory, 'less11')


@pytest.fixture(scope='module')
def fixture_file_bigger55(tmpdir_factory):
    return generate_fixture(tmpdir_factory, 'bigger55')


@pytest.fixture(scope='module')
def fixture_file_wrong_num_rows(tmpdir_factory):
    return generate_fixture(tmpdir_factory, 'wrong_num_rows')


@pytest.fixture(scope='module')
def fixture_file_wrong_num_cols(tmpdir_factory):
    return generate_fixture(tmpdir_factory, 'wrong_num_cols')
