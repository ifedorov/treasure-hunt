import pytest

from treasure_2 import load_table, find_treasure


def test_successful(fixture_file_successful):
    table = load_table(fixture_file_successful)
    assert ['11', '12', '13', '14', '15', '21',
            '22', '23', '24', '25', '31',
            '32', '33', '34', '35', '41',
            '42', '43', '44', '45', '51',
            '52', '53', '54', '55'] == find_treasure(array=table)


def test_successful_first(fixture_file_successful_first):
    table = load_table(fixture_file_successful_first)
    assert ['11'] == find_treasure(array=table)


def test_infinite(fixture_file_infinite_loop):
    table = load_table(fixture_file_infinite_loop)
    with pytest.raises(RuntimeError) as excinfo:
        find_treasure(array=table)
        assert "maximum recursion" in str(excinfo.value)


def test_clue_text(fixture_file_clue_text):
    table = load_table(fixture_file_clue_text)
    with pytest.raises(ValueError):
        find_treasure(array=table)


def test_clue_less_11(fixture_file_less11):
    table = load_table(fixture_file_less11)
    with pytest.raises(IndexError):
        find_treasure(array=table)


def test_clue_bigger_55(fixture_file_bigger55):
    table = load_table(fixture_file_bigger55)
    with pytest.raises(IndexError):
        find_treasure(array=table)
