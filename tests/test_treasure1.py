import pytest

from treasure_1 import Treasure, InfiniteLoop, WrongInitialData, ClueOutOfRange


def test_successful(fixture_file_successful):
    t = Treasure(fixture_file_successful)
    assert ['11', '12', '13', '14', '15', '21',
            '22', '23', '24', '25', '31',
            '32', '33', '34', '35', '41',
            '42', '43', '44', '45', '51',
            '52', '53', '54', '55'] == t.get_result()


def test_successful_first(fixture_file_successful_first):
    t = Treasure(fixture_file_successful_first)
    assert ['11'] == t.get_result()


def test_infinite(fixture_file_infinite_loop):
    t = Treasure(fixture_file_infinite_loop)
    with pytest.raises(InfiniteLoop):
        t.get_result()


def test_clue_text(fixture_file_clue_text):
    with pytest.raises(ValueError):
        Treasure(fixture_file_clue_text)


def test_clue_less_11(fixture_file_less11):
    with pytest.raises(ClueOutOfRange):
        Treasure(fixture_file_less11)


def test_clue_bigger_55(fixture_file_bigger55):
    with pytest.raises(ClueOutOfRange):
        Treasure(fixture_file_bigger55)


def test_wrong_num_rows(fixture_file_wrong_num_rows):
    with pytest.raises(WrongInitialData):
        Treasure(fixture_file_wrong_num_rows)


def test_wrong_num_cols(fixture_file_wrong_num_cols):
    with pytest.raises(WrongInitialData):
        Treasure(fixture_file_wrong_num_cols)
